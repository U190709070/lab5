public class GCDRec {

    public static void main(String[] args) {
        int n1= Integer.parseInt(args[0]);
        int n2= Integer.parseInt(args[1]);
        System.out.println(GCD(n1, n2));
    }
    public static int GCD(int a, int b){
        if (b != 0){
            return GCD(b, a % b);
        }else {
            return a;
        }
    }
}
