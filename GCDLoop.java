class GCDLoop {
      public static void main(String[] args) {
        int n1= Integer.parseInt(args[0]);
        int n2= Integer.parseInt(args[1]);
        System.out.println(GCD(n1, n2));
    }
    public static int GCD(int a, int b) {
        int gcd = 1;
        for (int i = 1; i <= a && i <= b; ++i) {
            if (a % i == 0 && b % i == 0)
                gcd = i;
        }
        return gcd;
    }

}  

