import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence {

    public static void main(String[] args) throws FileNotFoundException {
        int matrix[][] = readMatrix();

        boolean found = false;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (search(0, matrix, i, j)) {
                    if (check(matrix, i, j, 0)) {
                        found = true;
                        break;
                    }
                }

            }
        }

        if (found) {
            System.out.println("A sequence is found");
        }
        printMatrix(matrix);
    }

    public  static boolean check(int matrix[][], int row, int col, int number) {
        if (matrix[row][col] == 9) {
            return true;
        }else if (((row > 0) & (col > 0)) && (matrix[row - 1][col - 1] == number + 1 )) {
            matrix[row][col] = 9 - number;
            number++;
            return check(matrix, row - 1, col - 1, number);
        }else if ((row > 0) && (matrix[row - 1][col] == number + 1 )) {
            matrix[row][col] = 9 - number;
            number++;
            return check(matrix, row - 1, col, number);
        }else if (((row > 0) & (col < 9)) && (matrix[row - 1][col + 1] == number + 1 )) {
            matrix[row][col] = 9 - number;
            number++;
            return check(matrix, row - 1, col + 1, number);
        }else if ((col < 9) && (matrix[row][col + 1] == number + 1 )) {
            matrix[row][col] = 9 - number;
            number++;
            return check(matrix, row , col + 1, number);
        }else if ((col > 0) && (matrix[row][col - 1] == number + 1 )) {
            matrix[row][col] = 9 - number;
            number++;
            return check(matrix, row, col - 1, number );
        }else if (((row < 9) && (col > 0)) && (matrix[row + 1][col - 1] == number + 1)) {
            matrix[row][col] = 9 - number;
            number++;
            return check(matrix, row + 1, col - 1, number);
        }else if ((row < 9) && (matrix[row + 1][col] == number + 1 )) {
            matrix[row][col] = 9 - number;
            number++;
            return check(matrix, row + 1, col, number );
        }else if (((row < 9) && (col < 9)) && (matrix[row + 1][col + 1] == number + 1)) {
            matrix[row][col] = 9 - number;
            number++;
            return check(matrix, row + 1, col + 1, number);
        }
       return false;
    }

    private static boolean search (int number, int[][]matrix, int row, int col) {
        if (matrix[row][col] == 0) {
            return true;
        }

        return false;

    }

    private static int[][] readMatrix() throws FileNotFoundException{
        int[][] matrix = new int[10][10];
        File file = new File("/home/ondort/UniLab/lab5/lab5/matrix.txt");

        try (Scanner sc = new Scanner(file)){


            int i = 0;
            int j = 0;
            while (sc.hasNextLine()) {
                int number = sc.nextInt();
                matrix[i][j] = number;
                if (j == 9)
                    i++;
                j = (j + 1) % 10;
                if (i == 10)
                    break;
            }
        } catch (FileNotFoundException e) {
            throw e;
        }
        return matrix;
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }
    }
}